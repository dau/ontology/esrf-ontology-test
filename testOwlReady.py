from owlready2 import *
import os.path

path = "/users/koumouts/code/ontologies/Esrf2.rdf"
check_file = os.path.isfile(path)

print(check_file)

# onto = get_ontology("https://raw.githubusercontent.com/ExPaNDS-eu/ExPaNDS-experimental-techniques-ontology/master/releases/latest-release1/PaNET.owl")
onto = get_ontology("file:///users/koumouts/code/esrf-ontology/Esrf2.rdf").load()

# gcas = list(onto.general_class_axioms())
# print(gcas)
allBeamlines = onto.search(is_a=onto.Beamline)
print(allBeamlines)

data_list = []
for individual in allBeamlines:
    data_list.append(
        {
            "individual name": individual.name,
            "relation": "is_a",
            "class": "Beamline",
        }
    )

# Serialize the data list to JSON
import json

json_data = json.dumps(data_list, indent=2)

# Print or save the JSON data
print(json_data)

# graph = []
# for individual in allBeamlines:
#     graph.append({
#         "indevidual name": individual.name,
#         "relation": "is_a",
#         "class": "Beamline",
#     })

# # Serialize the data list to JSON
# import json
# json_graph = json.dumps(graph, indent=2)

# # Print or save the JSON data
# print(json_graph)


with onto:
    print(
        list(
            default_world.sparql(
                """
        PREFIX esrf: <http://www.semanticweb.org/koumouts/ontologies/2023/8/esrf-ontology#>
        SELECT ?beamline ?q
        WHERE {
            ?beamline a esrf:MX_Beamline .
            ?beamline esrf:utilizesTechnique ?q .
        }
    """
            )
        )
    )

    print(
        list(
            default_world.sparql(
                """
        PREFIX esrf: <http://www.semanticweb.org/koumouts/ontologies/2023/8/esrf-ontology#>
        SELECT ?q
        WHERE {
            esrf:esrf_ID21_beamline esrf:utilizesTechnique ?q .
        }
    """
            )
        )
    )

    print(
        list(
            default_world.sparql(
                """
        PREFIX esrf: <http://www.semanticweb.org/koumouts/ontologies/2023/8/esrf-ontology#>
        SELECT DISTINCT ?class ?objectProperty ?datatypeProperty
        WHERE {
        esrf:esrf_ID21_beamline rdf:type ?class .
        ?objectProperty rdf:type owl:ObjectProperty .
        ?datatypeProperty rdf:type owl:DatatypeProperty .
        }
    """
            )
        )
    )

# get all classes of the ontology and depicting their meaningful label
#  or owl:NamedIndividual
# print(list(default_world.sparql("""
#        SELECT ?class ?classLabel
#         WHERE {
#         ?class a owl:Class .
#         ?class rdfs:label ?classLabel.
#         }
# """)))
