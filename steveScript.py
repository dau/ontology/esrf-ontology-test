from owlready2 import *
# be careful - restart kernel for new owl file

def displayParentsChildren(panet_url, term):
    
    # panet_url: url or filename of ontology .owl file
    # technique: name, label or alt. label of PaNET term    

    panet = get_ontology(panet_url).load()
    with panet:
        
        sync_reasoner_pellet()
        
        panet_class = None
    
        for pncls in panet.classes():
            if term == pncls.name:
                panet_class = pncls
                break      
    
        # if term isn't a panet name then look for it in labels
        if panet_class == None:
            for pncls in panet.classes():
                if term in pncls.label or term in pncls.altLabel:
                    panet_class = pncls
                    break  
        
        if panet_class == None:
            raise NameError("=== error - can't find term '%s' as a panet name, label or alt. label" % term)
            
        parents = panet_class.mro()[0:-2]
        #parents = list(panet.get_parents_of(panet_class)) ## alternative - doesn't seemto work
        
        print('\nProperties of %s (Label: %s) in %s:\n' % (pncls, pncls.label[0], panet_url))

        # remove owl.thing (only present if no other direct superclasses)
        direct_superclasses = [cls for cls in panet_class.is_a if cls != owl.Thing] 
        
        print('\nSuperclasses (direct and given by equivalent classes):\n')
        displayClasses(direct_superclasses)   
        print('\nNo. of direct_superclasses of %s - label: %s: %i ' % (panet_class, panet_class.label, len(direct_superclasses)))
        print('\n' + '-'*50)
    
        print('\nAll superclasses:\n')
        displayClasses(parents)

        print('\nNo. of superclasses of %s (including self)- label: %s: %i ' % (panet_class, panet_class.label, len(parents)))
        print('\n' + '-'*50)
    
   
        subclasses = []
        for cls in panet.classes():
            if panet_class in cls.mro()[0:-2]:
                subclasses += [cls]

        #subclasses = list(panet.get_children_of(panet_class)) ### alternative -doesn't seem to work
    
        #subclasses = list(panet_class.subclasses())
        print('\nSubclasses (children):\n')
        displayClasses(subclasses)   
        print('\nNo. of subclasses of %s (including self): %i ' % (panet_class, len(subclasses)))
        print('\n' + '-'*50)


def displayClasses(classList):
    # order and print classes in class list, with all labels
    cls_list = []
    
    for cls in classList:
        label = ''
        if cls.label:
            label = cls.label[0]
        
        if cls.altLabel:
            altlabels = '(Alt. Labels: ' + ', '.join(cls.altLabel) + ')'
        else:
            altlabels = ''
        
        cls_list += ['%s %s %s' % (str(cls), label, altlabels)]
    
    cls_list.sort()
    for cls_str in cls_list:
        print(cls_str)



def findLabelClashes(panet_url):
    # Find and display duplicated labels  
        
    panet = get_ontology(panet_url).load()

    all_labels = []
    #all_labels = ['birefringence', 'MAD'] # for testing
    
    duplicate_labels = []
    
    for pncls in panet.classes():

        if not len(pncls.label) == 0: # ignore if no label
            for label in all_labels:
                if (label == pncls.label[0]) or (label in pncls.altLabel):
                    duplicate_labels += [label]
             
        all_labels += pncls.label
        all_labels += pncls.altLabel
 
    return(duplicate_labels)

def showClassesWithLabels(label_list, panet_url):
    # show PaNET classes that have label or altLabel in label_list
        
    panet = get_ontology(panet_url).load()
    
    for label in label_list:
        for pncls in panet.classes():
            if not len(pncls.label) == 0: # ignore if no label
                if (label == pncls.label[0]) or (label in pncls.altLabel):
                    print('Duplicate Label: %s\tPaNET Class: ' %  label, pncls, pncls.label[0], pncls.altLabel)

def showLabelClashes(file):
    # display classes with label clashes
    print('Label clashes in %s:' % file)
    clashes = findLabelClashes(file)
    if clashes == []:
        print('No clashes')
    else:
        print('Clashes found:')
        showClassesWithLabels(clashes, file)

#file = 'http://purl.org/pan-science/PaNET/PaNET.owl' # current release
#file = '/dls/science/users/spc93/git/ExPaNDS-experimental-techniques-ontology/source/PaNET.owl' # local test version
file = './PaNET.owl' # tmp test version
#file = '/dls/science/users/spc93/tmp/PaNET_reasoned.owl' # tmp test version reasoned
showLabelClashes(file)

displayParentsChildren(file, 'neutron single crystal diffraction') #112
displayParentsChildren(file, 'imaging') #115
displayParentsChildren(file, 'x-ray absorption spectroscopy') #118