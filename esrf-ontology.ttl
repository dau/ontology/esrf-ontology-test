@prefix : <http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix esrf_ontology: <http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#> .
@base <http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology/> .

<http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology> rdf:type owl:Ontology .

#################################################################
#    Object Properties
#################################################################

###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#changes
esrf_ontology:changes rdf:type owl:ObjectProperty .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#has_photon_energy_range
esrf_ontology:has_photon_energy_range rdf:type owl:ObjectProperty .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#has_polarization
esrf_ontology:has_polarization rdf:type owl:ObjectProperty ;
                               rdfs:domain esrf_ontology:x-rays ;
                               rdfs:range esrf_ontology:polarization .


#################################################################
#    Data properties
#################################################################

###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#atomic_distance_range
esrf_ontology:atomic_distance_range rdf:type owl:DatatypeProperty ;
                                    rdfs:domain esrf_ontology:diffraction_of_elastic_scattering ;
                                    rdfs:range xsd:float .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#atomic_distance_resolution
esrf_ontology:atomic_distance_resolution rdf:type owl:DatatypeProperty ;
                                         rdfs:label "delta-Q" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#atomic_distance_resolution_unit
esrf_ontology:atomic_distance_resolution_unit rdf:type owl:DatatypeProperty .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#atomic_distance_unit
esrf_ontology:atomic_distance_unit rdf:type owl:DatatypeProperty ;
                                   rdfs:domain esrf_ontology:diffraction_of_elastic_scattering ;
                                   rdfs:range xsd:string .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#energy_range_unit
esrf_ontology:energy_range_unit rdf:type owl:DatatypeProperty ;
                                rdfs:range xsd:string ;
                                rdfs:label "has energy range unit" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#energy_range_value
esrf_ontology:energy_range_value rdf:type owl:DatatypeProperty ;
                                 rdfs:range xsd:float ;
                                 rdfs:label "has energy range value" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#sample_spatial_resolution
esrf_ontology:sample_spatial_resolution rdf:type owl:DatatypeProperty .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#sample_spatial_resolution_unit
esrf_ontology:sample_spatial_resolution_unit rdf:type owl:DatatypeProperty .


#################################################################
#    Classes
#################################################################

###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#DAC
esrf_ontology:DAC rdf:type owl:Class ;
                  rdfs:subClassOf esrf_ontology:sample_input .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#UV
esrf_ontology:UV rdf:type owl:Class ;
                 rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#angular_dispersive_detection_mode
esrf_ontology:angular_dispersive_detection_mode rdf:type owl:Class ;
                                                rdfs:subClassOf esrf_ontology:detection_mode .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#anomalous
esrf_ontology:anomalous rdf:type owl:Class ;
                        rdfs:subClassOf esrf_ontology:other ;
                        rdfs:comment "eg anomalous_SAXS" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#atomic_core_excitation
esrf_ontology:atomic_core_excitation rdf:type owl:Class ;
                                     rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#auger_electrons
esrf_ontology:auger_electrons rdf:type owl:Class ;
                              rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#biological_sample
esrf_ontology:biological_sample rdf:type owl:Class ;
                                rdfs:subClassOf esrf_ontology:sample_type .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#detection
esrf_ontology:detection rdf:type owl:Class ;
                        rdfs:subClassOf esrf_ontology:technique_property ;
                        rdfs:comment "Cascade of events being detected" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#detection_energy_range
esrf_ontology:detection_energy_range rdf:type owl:Class ;
                                     rdfs:subClassOf esrf_ontology:photon_energy .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#detection_mode
esrf_ontology:detection_mode rdf:type owl:Class ;
                             rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#diffraction
esrf_ontology:diffraction rdf:type owl:Class ;
                          rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#diffraction_of_elastic_scattering
esrf_ontology:diffraction_of_elastic_scattering rdf:type owl:Class ;
                                                rdfs:subClassOf esrf_ontology:diffraction .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#diffraction_of_photo-electrons
esrf_ontology:diffraction_of_photo-electrons rdf:type owl:Class ;
                                             rdfs:subClassOf esrf_ontology:diffraction .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#electrons
esrf_ontology:electrons rdf:type owl:Class ;
                        rdfs:subClassOf esrf_ontology:sample_input .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#energy_dispersive_detection_mode
esrf_ontology:energy_dispersive_detection_mode rdf:type owl:Class ;
                                               rdfs:subClassOf esrf_ontology:detection_mode .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#experimental_technique
esrf_ontology:experimental_technique rdf:type owl:Class .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#extended
esrf_ontology:extended rdf:type owl:Class ;
                       rdfs:subClassOf esrf_ontology:detection_energy_range .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#fluoresence
esrf_ontology:fluoresence rdf:type owl:Class ;
                          rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#hard_x-ray
esrf_ontology:hard_x-ray rdf:type owl:Class ;
                         owl:equivalentClass [ owl:intersectionOf ( esrf_ontology:named_photon_energy_ranges
                                                                    [ rdf:type owl:Restriction ;
                                                                      owl:onProperty esrf_ontology:has_photon_energy_range ;
                                                                      owl:someValuesFrom [ owl:intersectionOf ( esrf_ontology:photon_energy_range
                                                                                                                [ rdf:type owl:Restriction ;
                                                                                                                  owl:onProperty esrf_ontology:energy_range_value ;
                                                                                                                  owl:someValuesFrom [ rdf:type rdfs:Datatype ;
                                                                                                                                       owl:onDatatype xsd:float ;
                                                                                                                                       owl:withRestrictions ( [ xsd:minInclusive "10.0"^^xsd:float
                                                                                                                                                              ]
                                                                                                                                                              [ xsd:maxInclusive "100000.0"^^xsd:float
                                                                                                                                                              ]
                                                                                                                                                            )
                                                                                                                                     ]
                                                                                                                ]
                                                                                                                [ rdf:type owl:Restriction ;
                                                                                                                  owl:onProperty esrf_ontology:energy_range_unit ;
                                                                                                                  owl:hasValue "eV"
                                                                                                                ]
                                                                                                              ) ;
                                                                                           rdf:type owl:Class
                                                                                         ]
                                                                    ]
                                                                  ) ;
                                               rdf:type owl:Class
                                             ] ;
                         rdfs:comment "Defined using an object property (has_photon_energy_range) to connect to another class (photon_energy_range) that has the data properties (value and unit). It creates a chain where the characteristics of other classes included in the definition define the class. Works with the reasoner!" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#horizontal_linear_polarization
esrf_ontology:horizontal_linear_polarization rdf:type owl:Class ;
                                             rdfs:subClassOf esrf_ontology:polarization .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#infrared
esrf_ontology:infrared rdf:type owl:Class ;
                       rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#laser
esrf_ontology:laser rdf:type owl:Class ;
                    rdfs:subClassOf esrf_ontology:sample_input .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#left_circular_polarization
esrf_ontology:left_circular_polarization rdf:type owl:Class ;
                                         rdfs:subClassOf esrf_ontology:polarization .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#macro_sample_spatial_resolution
esrf_ontology:macro_sample_spatial_resolution rdf:type owl:Class ;
                                              owl:equivalentClass [ owl:intersectionOf ( esrf_ontology:sample_spatial_resolution
                                                                                         [ rdf:type owl:Restriction ;
                                                                                           owl:onProperty esrf_ontology:sample_spatial_resolution ;
                                                                                           owl:someValuesFrom [ rdf:type rdfs:Datatype ;
                                                                                                                owl:onDatatype xsd:float ;
                                                                                                                owl:withRestrictions ( [ xsd:minExclusive "10.0"^^xsd:float
                                                                                                                                       ]
                                                                                                                                     )
                                                                                                              ]
                                                                                         ]
                                                                                         [ rdf:type owl:Restriction ;
                                                                                           owl:onProperty esrf_ontology:sample_spatial_resolution_unit ;
                                                                                           owl:hasValue "micron"
                                                                                         ]
                                                                                       ) ;
                                                                    rdf:type owl:Class
                                                                  ] .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#magnetic_field
esrf_ontology:magnetic_field rdf:type owl:Class ;
                             rdfs:subClassOf esrf_ontology:sample_input .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#micro_sample_spatial_resolution
esrf_ontology:micro_sample_spatial_resolution rdf:type owl:Class ;
                                              rdfs:subClassOf esrf_ontology:sample_spatial_resolution .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#monochromatic_beam
esrf_ontology:monochromatic_beam rdf:type owl:Class ;
                                 rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#named_photon_energy_ranges
esrf_ontology:named_photon_energy_ranges rdf:type owl:Class ;
                                         rdfs:subClassOf esrf_ontology:photon_energy .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#nano_sample_spatial_resolution
esrf_ontology:nano_sample_spatial_resolution rdf:type owl:Class ;
                                             rdfs:subClassOf esrf_ontology:sample_spatial_resolution .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#near_edge
esrf_ontology:near_edge rdf:type owl:Class ;
                        rdfs:subClassOf esrf_ontology:detection_energy_range .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#other
esrf_ontology:other rdf:type owl:Class ;
                    rdfs:subClassOf esrf_ontology:technique_property .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#photo_electrons
esrf_ontology:photo_electrons rdf:type owl:Class ;
                              rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#photon_energy
esrf_ontology:photon_energy rdf:type owl:Class ;
                            rdfs:subClassOf esrf_ontology:sample_input .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#photon_energy_range
esrf_ontology:photon_energy_range rdf:type owl:Class ;
                                  owl:equivalentClass [ rdf:type owl:Restriction ;
                                                        owl:onProperty esrf_ontology:energy_range_value ;
                                                        owl:someValuesFrom xsd:float
                                                      ] ;
                                  rdfs:subClassOf esrf_ontology:technique_property .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#pink_beam
esrf_ontology:pink_beam rdf:type owl:Class ;
                        rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#polarization
esrf_ontology:polarization rdf:type owl:Class ;
                           rdfs:subClassOf esrf_ontology:sample_input .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#powder_sample
esrf_ontology:powder_sample rdf:type owl:Class ;
                            rdfs:subClassOf esrf_ontology:sample_type .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#right_circular_polarization
esrf_ontology:right_circular_polarization rdf:type owl:Class ;
                                          rdfs:subClassOf esrf_ontology:polarization .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#sample_input
esrf_ontology:sample_input rdf:type owl:Class ;
                           rdfs:subClassOf esrf_ontology:technique_property .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#sample_spatial_resolution
esrf_ontology:sample_spatial_resolution rdf:type owl:Class ;
                                        rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#sample_spatial_resolution_achieved_on_detection
esrf_ontology:sample_spatial_resolution_achieved_on_detection rdf:type owl:Class ;
                                                              rdfs:subClassOf esrf_ontology:sample_spatial_resolution .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#sample_spatial_resolution_achived_on_input
esrf_ontology:sample_spatial_resolution_achived_on_input rdf:type owl:Class ;
                                                         rdfs:subClassOf esrf_ontology:sample_spatial_resolution .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#sample_type
esrf_ontology:sample_type rdf:type owl:Class ;
                          rdfs:subClassOf esrf_ontology:technique_property .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#scanning
esrf_ontology:scanning rdf:type owl:Class ;
                       rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#serial
esrf_ontology:serial rdf:type owl:Class ;
                     rdfs:subClassOf esrf_ontology:other ;
                     rdfs:comment "eg serial_crystallography" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#single_crystal_sample
esrf_ontology:single_crystal_sample rdf:type owl:Class ;
                                    rdfs:subClassOf esrf_ontology:sample_type .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#soft_x-ray
esrf_ontology:soft_x-ray rdf:type owl:Class ;
                         owl:equivalentClass [ owl:intersectionOf ( esrf_ontology:named_photon_energy_ranges
                                                                    [ rdf:type owl:Restriction ;
                                                                      owl:onProperty esrf_ontology:energy_range_value ;
                                                                      owl:someValuesFrom [ rdf:type rdfs:Datatype ;
                                                                                           owl:onDatatype xsd:float ;
                                                                                           owl:withRestrictions ( [ xsd:minInclusive "5.0"^^xsd:float
                                                                                                                  ]
                                                                                                                )
                                                                                         ]
                                                                    ]
                                                                    [ rdf:type owl:Restriction ;
                                                                      owl:onProperty esrf_ontology:energy_range_unit ;
                                                                      owl:hasValue "eV"
                                                                    ]
                                                                  ) ;
                                               rdf:type owl:Class
                                             ] ;
                         rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#technique_property
esrf_ontology:technique_property rdf:type owl:Class .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#tomography
esrf_ontology:tomography rdf:type owl:Class ;
                         rdfs:subClassOf esrf_ontology:other ;
                         rdfs:comment "rotate the sample with respect to detector+beam or vice-versa" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#total_electrons
esrf_ontology:total_electrons rdf:type owl:Class ;
                              owl:equivalentClass [ rdf:type owl:Class ;
                                                    owl:unionOf ( esrf_ontology:auger_electrons
                                                                  esrf_ontology:photo_electrons
                                                                )
                                                  ] ;
                              rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#total_reflection
esrf_ontology:total_reflection rdf:type owl:Class ;
                               rdfs:subClassOf esrf_ontology:other ;
                               rdfs:comment "angle with sample surface < critical angle" .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#transmission
esrf_ontology:transmission rdf:type owl:Class ;
                           rdfs:subClassOf esrf_ontology:detection .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#vertical_linear_polarization
esrf_ontology:vertical_linear_polarization rdf:type owl:Class ;
                                           rdfs:subClassOf esrf_ontology:polarization .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#visual_spectrum
esrf_ontology:visual_spectrum rdf:type owl:Class ;
                              rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#white_beam
esrf_ontology:white_beam rdf:type owl:Class ;
                         rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#x-ray
esrf_ontology:x-ray rdf:type owl:Class ;
                    rdfs:subClassOf esrf_ontology:named_photon_energy_ranges .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#x-rays
esrf_ontology:x-rays rdf:type owl:Class ;
                     rdfs:subClassOf esrf_ontology:sample_input .


#################################################################
#    Individuals
#################################################################

###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#changeOverSpace
esrf_ontology:changeOverSpace rdf:type owl:NamedIndividual .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#changeOverTime
esrf_ontology:changeOverTime rdf:type owl:NamedIndividual .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#hardXrays
esrf_ontology:hardXrays rdf:type owl:NamedIndividual ,
                                 esrf_ontology:named_photon_energy_ranges ;
                        esrf_ontology:has_photon_energy_range esrf_ontology:photon_Energy_Range_For_Hard_Xrays .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#photon_Energy_Range_For_Hard_Xrays
esrf_ontology:photon_Energy_Range_For_Hard_Xrays rdf:type owl:NamedIndividual ;
                                                 esrf_ontology:energy_range_unit "eV" ;
                                                 esrf_ontology:energy_range_value "11.0"^^xsd:float .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#softXrays
esrf_ontology:softXrays rdf:type owl:NamedIndividual ,
                                 esrf_ontology:named_photon_energy_ranges ;
                        esrf_ontology:energy_range_unit "eV" ;
                        esrf_ontology:energy_range_value "6.0"^^xsd:float .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#testChangeOverSpaceET
esrf_ontology:testChangeOverSpaceET rdf:type owl:NamedIndividual ,
                                             esrf_ontology:x-rays ;
                                    esrf_ontology:changes esrf_ontology:changeOverSpace ,
                                                          esrf_ontology:changeOverTime .


###  http://www.semanticweb.org/koumouts/ontologies/2024/3/esrf_ontology#testEAFS
esrf_ontology:testEAFS rdf:type owl:NamedIndividual ,
                                esrf_ontology:atomic_core_excitation ,
                                esrf_ontology:experimental_technique ;
                       esrf_ontology:energy_range_value "2.0"^^xsd:float .


###  Generated by the OWL API (version 4.5.26.2023-07-17T20:34:13Z) https://github.com/owlcs/owlapi
